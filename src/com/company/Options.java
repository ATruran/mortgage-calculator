package com.company;

import java.text.DecimalFormat;
import java.util.Scanner;

public class Options {
    private Double total;
    private float rate;
    private int duration;
    private String firstname;
    private String surname;

    public Options(double total, float rate,int duration, String firstname, String surname) {
        this.total = total;
        this.rate = rate;
        this.duration = duration;
        this.firstname = firstname;
        this.surname = surname;
    }

    // to create the get and set methods select them right click then generate then set getter and setter
    public double getTotal() {
        return total;
    }
    public void setTotal(double total) {
        this.total = total;
    }

    public float getRate() {
        return rate;
    }
    public void setRate(float rate) {
        this.rate = rate;
    }

    public String getFirstname() {
        return firstname;
    }
    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getSurname() {
        return surname;
    }
    public void setSurname(String surname) {
        this.surname = surname;
    }

    public int getDuration() {
        return duration;
    }
    public void setDuration(int duration) {
        this.duration = duration;
    }

    public boolean userInput(){
        DecimalFormat df = new DecimalFormat("#.##");
        Scanner scan = new Scanner(System.in);
        System.out.print("Do you wish to change the default settings: (y/n)");
        String option = scan.nextLine();
        Scanner changes = new Scanner(System.in);
        Scanner rerun = new Scanner(System.in);
        if (option.equals("y") || option.equals("Y")){
            System.out.println("Default names: " + "first_name: " + firstname + " surname: "+surname);
            System.out.println("Default Values: "+"total: £"+total+" rate "+rate+"% repayment_duration: "+duration+"years"+'\n');
            int i = 0;
            while (i == 0)
            {

                System.out.print("What values do you wish to change: (enter in lowercase in the format shown above)");
                String option2 = changes.nextLine();
                if (option2.toLowerCase().equals("first_name") || option2.toLowerCase().equals("firstname")){
                    System.out.print("please enter the new first name");
                    String adjustments = changes.nextLine();
                    this.setFirstname(adjustments);
                }
                else if (option2.toLowerCase().equals("surname")){
                    System.out.print("please enter the new surname");
                    String adjustments = changes.nextLine();
                    this.setSurname(adjustments);
                }
                else if (option2.toLowerCase().equals("total")){
                    System.out.print("please enter the new total value(just numbers)");
                    double value = changes.nextDouble();
                    this.setTotal(value);
                }
                else if (option2.toLowerCase().equals("rate")){
                    System.out.print("please enter the new interest rate (just numbers)");
                    float value = changes.nextFloat();
                    this.setRate(value);
                }
                else if (option2.toLowerCase().equals("repayment_duration")){
                    System.out.print("please enter the new repayment duration (Just numbers)");
                    int years = changes.nextInt();
                    this.setDuration(years);
                }
                else{System.out.println("Invalid Input");}

              //  Scanner rerun = new Scanner(System.in);
                System.out.println("do you wish to make another change? (Y/N)");
                char finalchoice = rerun.next().charAt(0);
                switch (Character.toLowerCase(finalchoice)){
                    case 'y':
                        System.out.println("Current Names: " + "first_name: " + firstname + " surname: "+surname);
                        System.out.println("Current Values: "+"total: £"+total+" rate "+rate+"% repayment_duration: "+duration+"years");
                        i = 0;
                        break;
                    case 'n':
                        i++;
                        System.out.println("Names: " + "first_name: " + firstname + " surname: "+surname);
                        System.out.println("Values: "+"total: £"+total+" rate "+rate+"% repayment_duration: "+duration+"years");
                        changes.close();
                        rerun.close();
                        break;
                    default:
                        System.out.println("invalid input closing options");
                        break;
                }
              //  changes.close();
            }
            // this.setDuration(<value>) to change duration
        }
        else {
            return false;
        }
        Mortgage mortgage = new Mortgage("Fixed Term Mortgage", this.total, this.rate,this.duration, this.firstname,this.surname);
        mortgage.setTotal(total);
        mortgage.setInterestRate(rate);
        mortgage.setDuration(duration);
        mortgage.setClientFirstName(firstname);
        mortgage.setClientSecondName(surname);
        mortgage.setName("House Mortgage"); //call to the setName method in the Mortgage class
        mortgage.processTofile();
        scan.close();
        return true;
    }
    //on fourth and fifth input loop it does the invalid input jump (total and rate)
}
