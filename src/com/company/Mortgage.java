package com.company;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.text.DecimalFormat;
import java.util.Scanner;

//TODO finish off this class and use it in main

public class Mortgage {

    private String Name;
    private double total;
    private float interestRate;
    private int duration;
    private String clientFirstName;
    private String clientSecondName;

    public Mortgage(String name, double total, float interestRate, int duration, String clientFirstName, String clientSecondName) {
        Name = name;
        this.total = total;
        this.interestRate = interestRate;
        this.duration = duration;
        this.clientFirstName = clientFirstName;
        this.clientSecondName = clientSecondName;
    }

    public String getName() {
        return Name;
    }
    public void setName(String name) {
        Name = name;
    }

    public double getTotal() {
        return total;
    }
    public void setTotal(double total) {
        this.total = total;
    }

    public float getInterestRate() {
        return interestRate;
    }
    public void setInterestRate(float interestRate) {
        this.interestRate = interestRate;
    }

    public int getDuration() {
        return duration;
    }
    public void setDuration(int duration) {
        this.duration = duration;
    }

    public String getClientFirstName() {
        return clientFirstName;
    }
    public void setClientFirstName(String clientFirstName) {
        this.clientFirstName = clientFirstName;
    }

    public String getClientSecondName() {
        return clientSecondName;
    }
    public void setClientSecondName(String clientSecondName) {
        this.clientSecondName = clientSecondName;
    }

    public void processTofile() {
        DecimalFormat df = new DecimalFormat("#.##");
        double newbal;
        double im = (this.interestRate / 12) / 100;
        int nm = this.duration * 12;
        double mp, ip, pp;
        int i;

        mp = this.total * im * Math.pow(1 + im, (double) nm) / (Math.pow(1 + im, (double) nm) - 1);
        //print amortization schedule for all months except the last month
        try {
            PrintWriter pw = new PrintWriter(new File("exampletest.csv"));
            StringBuilder sb = new StringBuilder();
            //could give it a method to itself
            sb.append(getName()+','+','+getClientFirstName()+' '+getClientSecondName()+'\n');
            sb.append("month"+','+"principle"+','+"Monthly Payment"+','+"intrest paid"+','+"principle paid"+','+"New Balance"+'\n');
            double ob = this.total;
            for (i = 1; i < nm; i++) {
                ip = this.total * im;//interest paid
                pp = mp - ip; //princial paid
                newbal = this.total - pp; //new balance
                this.total = newbal;  //update old balance
                sb.append(df.format(i) + ',' + df.format(ob) + ',' + df.format(mp) + ',' + df.format(ip) + ',' + df.format(pp) + ',' + df.format(newbal) + '\n');
            }
            //last month
            pp = this.total;
            ip = this.total * im;
            mp = pp + ip;
            newbal = 0.0;
            sb.append(df.format(i) + ',' + df.format(this.total) + ',' + df.format(mp) + ',' + df.format(ip) + ',' + df.format(pp) + ',' + df.format(newbal) + '\n');
            pw.write(sb.toString());
            pw.close();
        } catch (FileNotFoundException e) {
            System.out.println("Invalid Filename");
        }


    }
}
