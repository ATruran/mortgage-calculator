package com.company;
import javax.naming.Name;
import java.io.*;
import java.text.DecimalFormat;
import java.util.Scanner;
//the amount paid by the borrower every month that ensures that the loan is paid off in full
//The monthly payment formula is based on the annuity formula
// readdouble for floats

public class Main {
    public static void main(String args[]) {
        Mortgage mortgage = new Mortgage("Fixed Term Mortgage", 250000, 3,40,"John","Doe");
        //String name, float total, float interestRate, int duration, String clientFirstName, String clientSecondName)
        double total = mortgage.getTotal();
        float rate = mortgage.getInterestRate();
        int duration = mortgage.getDuration();
        String firstname =  mortgage.getClientFirstName();
        String surname = mortgage.getClientSecondName();
        Options options = new Options(total,rate, duration,firstname,surname);
        options.userInput();
    }
}
        /*float amount = 0, interest = 0;
        int period = 0;

        System.out.println("Please enter the morgage amount, interest rate and repayment period in years seperated by a space (dont include symbols)");
        Scanner numbers = new Scanner(System.in);
        String line = numbers.nextLine();
        String[] elements = line.split(" ");

        if (elements.length == 3) {
            try {
                amount = Float.parseFloat(elements[0]);
            } catch (NumberFormatException morgageamount) {
                // The number did not parse for some reason
                System.out.println("first number was invalid \n");
                System.out.println("please enter a valid calculation");
            }
            try {
                interest = Float.parseFloat(elements[1]);
            } catch (NumberFormatException interestrate) {
                // The number did not parse for some reason
                System.out.println("second number was invalid \n");
                System.out.println("please enter a valid calculation");
            }
            try {
                period = Integer.parseInt(elements[2]);
            } catch (NumberFormatException repaymentperiod) {
                System.out.println("Invalid repayment period\n");
                System.out.println("please enter a valid period (in whole years)");
            }
            System.out.println("£" + amount + " " + interest + "% " + period + " years");
        }
        calculation(amount, interest, period);
        CSV(amount, interest, period);
    }

    public static void calculation( float a, float i, float y) {
        for (int c = 1; c <= y; c++) {
            float rate = a * (i / 100);
            float total = a + rate; //total amount including rate
        }
        DecimalFormat df = new DecimalFormat("#.##");
        float monthlyinterest = (i/12)/100;
        float m = y * 12;
        double mp = a*monthlyinterest*Math.pow(1+monthlyinterest,(double)m)/(Math.pow(1+monthlyinterest,(double)m)-1);
        System.out.println("Monthly Installments £" + df.format(mp));
    }

    // figure out why it doesnt change the output, adjust the calculations so that they modify the data


    //OUTPUT TABLE TO CSV month payment total
    public static void CSV(float a, float i, int y) { // amount, interest rate, years
        DecimalFormat df = new DecimalFormat("#.##");
        float nb;// new and old balance
        float mi = (i / 12) / 100; // monthly interest
        int m = y * 12; // number of months
        float ip, ap;// mp: monthly payment, ip: interest paid, ap: amount paid
        //calculate monthly payments
        double mp = a*mi*Math.pow(1+mi,(double)m)/(Math.pow(1+mi,(double)m)-1);
        // print the CSV amortization table before all months except the last
        //printHeader();
        try{
            PrintWriter  pw = new PrintWriter(new File("/output-files/Mortgage.csv"));
            StringBuilder sb = new StringBuilder();
            sb.append("month"+','+"Monthly Payment"+','+"New Balance"+'\n');
            for (int c = 1; c < m; c++) {
                ip = a * mi; //interest paid, amount * monthly interest
                ap = (float) mp - ip; //amount paid, monthly payments - interest paid
                nb = a - ap;
                sb.append(df.format(c)+','+df.format(mp)+','+df.format(nb)+'\n');
                a = nb;
            }
            //last month
            ap = a;
            ip = a * mi;
            nb = 0;
            sb.append(df.format(m)+','+df.format(mp)+','+df.format(nb)+'\n');
            pw.write(sb.toString());
            pw.close();
        }
        catch (FileNotFoundException e) {
            System.out.println("Invalid Filename");
        }
    }*/
//}